package app.com.utils.utils;


/**
 * <p>Класс перемещает файлы из ассетов в хранилище</p>
 */
public class Mover {

    /**
     * пути к папкам, которые нужно перенести вместе с их содержимым в хранилище приложения
     */
    private String[] paths = new String[]{"fonts", "js/libs"};

    /**
     * Инициализация
     */
    public Mover(){
        moveAll();
    }

    /**
     * Перемещает все папки
     */
    public void moveAll(){

    }

    /**
     * Перемещает папку с ассетов в хранилище по указанному пути
     * начальный относительный путь = конченому относительному пути
     * @param path Путь к папке
     */
    public void move(String path){

    }
}
